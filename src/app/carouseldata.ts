export const carouseldata = {

    carousel: [

       { cityname:'Maldives',citydescription:'Maldives, also referred to as the Maldive Islands, is an island nation in the Indian Ocean.', cityimage:'img3.jpeg'},
       { cityname:'Paris',citydescription:'Paris has the best boulevards,museums and boutiques.<i>Eifel Tower </i>is the pride of this place.', cityimage:'img2.jpeg'},
       { cityname:'New York',citydescription:'Explore New York City holidays and discover the best time and places to visit.', cityimage:'img5.jpeg'},
       { cityname:'Amsterdam',citydescription:'Amsterdam is the ideal place for a one of a kind city trip', cityimage:'img1.jpeg'},
       { cityname:'Crotia',citydescription:'If your Mediterranean fantasies feature balmy days by sapphire waters in the shade of ancient walled towns', cityimage:'img4.jpeg'}
       
        
    ]
};