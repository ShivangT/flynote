import { Component, OnInit } from '@angular/core';
import { ConfigService } from '../config.service';

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.scss']
})
export class CarouselComponent implements OnInit {
    carousel = { };
  constructor(private config: ConfigService) { }

  ngOnInit() {
    this.carousel = this.getCarousel();
  }
  getCarousel(){
    // console.log(this.config.config)
    return this.config.config;
  }
}
