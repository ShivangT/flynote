import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})

export class FormComponent implements OnInit {

  model:any = {}
  constructor(private httpClient:HttpClient) { }

  ngOnInit() {
  }
  onSubmit() {
    
    var formData = new FormData();

    for (let k in this.model) {
      console.log(k);
      console.log(this.model[k]);
      formData.append(k, this.model[k]);
  }
    console.log('logging model: ', JSON.stringify(this.model));
    var dataVal = `phone=${this.model.phone}&email=${this.model.email}&avatar=${this.model.avatar}`;
    console.log(typeof(dataVal))
  

  this.httpClient.post("https://about2fly.flynote.in/api/admin/intern", dataVal,
        {
          
          headers: {
            
                'Content-Type': 'application/x-www-form-urlencoded'
              },
              
        })
        .subscribe(
            data => {
                console.log("POST Request is successful ", data);
            },
            error => {
                console.log("Error", error);
            }
        );       
  } 
}
